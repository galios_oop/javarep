import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;

public class PhrasesTest {

    @org.junit.Test
    public void get_list() {

        String str = "we all live in a yellow submarine";
        InputStream stream = new ByteArrayInputStream(str.getBytes());

        ArrayList<String> list = new ArrayList<>();
        list.add("we");
        list.add("all");
        list.add("live");
        list.add("in");
        list.add("a");
        list.add("yellow");
        list.add("submarine");

        ArrayList<String> arr = Phrases.get_list(stream);
        assertEquals(list, arr);
    }

    @org.junit.Test
    public void get_phrases() {

        ArrayList<String> list = new ArrayList<>();
        list.add("we");
        list.add("all");
        list.add("live");
        list.add("in");
        list.add("a");
        list.add("yellow");
        list.add("submarine");

        Map<String, Integer> phrases = new HashMap<>();
        phrases.put("we all", 1);
        phrases.put("all live", 1);
        phrases.put("live in", 1);
        phrases.put("in a", 1);
        phrases.put("a yellow", 1);
        phrases.put("yellow submarine", 1);

        Map<String, Integer> phrases2 = new HashMap<>();
        phrases2.put("we all live", 1);
        phrases2.put("all live in", 1);
        phrases2.put("live in a", 1);
        phrases2.put("in a yellow", 1);
        phrases2.put("a yellow submarine", 1);

        Map<String, Integer> m = Phrases.get_phrases(2, list);
        Map<String, Integer> m2 = Phrases.get_phrases(3, list);

        assertEquals(m, phrases);
        assertEquals(m2, phrases2);
    }

    @org.junit.Test
    public void sort_phrases() {

        Map<String, Integer> phrases = new HashMap<>();
        phrases.put("we all", 2);
        phrases.put("all live", 3);
        phrases.put("live in", 4);
        phrases.put("in a", 1);
        phrases.put("a yellow", 2);
        phrases.put("yellow submarine", 3);

        ArrayList<Map.Entry<String, Integer>> sorted = new ArrayList<>();
        Map.Entry<String, Integer> entry = new HashMap.SimpleEntry<>("live in", 4);
        sorted.add(entry);

        ArrayList<Map.Entry<String, Integer>> sorted2 = new ArrayList<>();
        sorted2.add(entry);
        entry = new HashMap.SimpleEntry<>("yellow submarine", 3);
        sorted2.add(entry);
        entry = new HashMap.SimpleEntry<>("all live", 3);
        sorted2.add(entry);

        ArrayList<Map.Entry<String, Integer>> res = Phrases.sort_phrases(4, phrases);
        ArrayList<Map.Entry<String, Integer>> res2 = Phrases.sort_phrases(3, phrases);

        assertEquals(res, sorted);
        assertEquals(res2, sorted2);
    }
}