import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class Phrases {

    public static void main(String[] args) {
        try {
            int len = args.length;

            int n = 2, m = 2;

            ArrayList<String> words;
            Map<String, Integer> phrases;
            ArrayList<Map.Entry<String, Integer>> sorted = new ArrayList<>();

            switch (len) {
                case 1: {
                    if (args[0].equals("-")) {
                        System.out.print("Write strings:\n");
                        words = get_list(System.in);
                    } else {
                        FileInputStream ist = new FileInputStream(args[0]);
                        words = get_list(ist);
                        ist.close();
                    }

                    phrases = get_phrases(n, words);
                    sorted = sort_phrases(m, phrases);

                    break;
                }
                case 3: {
                    switch (args[0]) {
                        case "-n":
                            n = Integer.parseInt(args[1]);
                            break;
                        case "-m":
                            m = Integer.parseInt(args[1]);
                            break;
                        default:
                            break;
                    }

                    if (args[2].equals("-")) {
                        System.out.print("Write strings:\n");
                        words = get_list(System.in);
                    } else {
                        FileInputStream ist = new FileInputStream(args[2]);
                        words = get_list(ist);
                        ist.close();
                    }

                    phrases = get_phrases(n, words);
                    sorted = sort_phrases(m, phrases);

                    break;
                }
                case 5: {
                    switch (args[0]) {
                        case "-n":
                            n = Integer.parseInt(args[1]);
                            if (args[2].equals("-m")) {
                                m = Integer.parseInt(args[3]);
                            }
                            break;
                        case "-m":
                            m = Integer.parseInt(args[1]);
                            if (args[2].equals("-n")) {
                                n = Integer.parseInt(args[3]);
                            }
                            break;
                        default:
                            break;
                    }

                    if (args[4].equals("-")) {
                        System.out.print("Write strings:\n");
                        words = get_list(System.in);
                    } else {
                        FileInputStream ist = new FileInputStream(args[4]);
                        words = get_list(ist);
                        ist.close();
                    }

                    phrases = get_phrases(n, words);
                    sorted = sort_phrases(m, phrases);

                    break;
                }
                default:
                    break;
            }

            for (Map.Entry<String, Integer> i : sorted) {
                System.out.print(i.getKey());
                System.out.print(" (");
                System.out.print(i.getValue());
                System.out.println(")");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> get_list(InputStream ist) {
        Scanner s = new Scanner(ist).useDelimiter("\\A");
        String result = s.hasNext() ? s.next() : "";

        result = result.replaceAll("\\s+", " ");
        String[] str = result.split(" ");

        return new ArrayList<>(Arrays.asList(str));
    }

    public static Map<String, Integer> get_phrases(int n, ArrayList<String> words) {
        Map<String, Integer> phrases = new HashMap<>();
        String phrase = "";
        int size = words.size();

        for (int i = 0; (i + n) <= size; i++) {
            for (int j = i; j < i + n; j++) {
                if (j != i + n - 1) phrase += words.get(j) + ' ';
                else phrase += words.get(j);
            }
            int count = phrases.getOrDefault(phrase, 0);
            phrases.put(phrase, count + 1);

            phrase = "";
        }

        return phrases;
    }

    public static ArrayList<Map.Entry<String, Integer>> sort_phrases(int m, Map<String, Integer> phrases) {
        ArrayList<Map.Entry<String, Integer>> sorted = new ArrayList<>();

        for (Map.Entry<String, Integer> i : phrases.entrySet()) {
            if (i.getValue() >= m) {
                sorted.add(i);
            }
        }
        sorted.sort((o1, o2) -> {
            if (o1.getValue() > o2.getValue()) {
                return -1;
            } else if (o1.getValue().equals(o2.getValue())) {
                return 0;
            } else {
                return 1;
            }
        });

        return sorted;
    }
}

