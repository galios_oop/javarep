import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class Client {

    private static String username;
    private ArrayList<String> users = new ArrayList<>();
    private JTextArea receive;
    private JTextArea userList;
    private JTextField send;

    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Client client = new Client();
        System.out.print("Write your username: ");
        username = scanner.nextLine();
        client.start();
    }

    private void start() {
        JFrame frame = new JFrame("Chat");
        JPanel panel = new JPanel();
        frame.setVisible(true);
        receive = new JTextArea(20, 50);
        userList = new JTextArea(5, 50);

        receive.setLineWrap(true);
        receive.setWrapStyleWord(true);
        receive.setEditable(false);
        userList.setEditable(false);

        JScrollPane scroller = new JScrollPane(receive);
        scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        send = new JTextField(20);
        JButton sendBut = new JButton("Send");
        sendBut.addActionListener(e -> {
            try {
                outputStream.writeObject(username);
                outputStream.writeObject(": " + send.getText());
            } catch (Exception ex) {
                System.out.println("Can't send message to the server");
            }
            send.setText("");
            send.requestFocus();
        });

        panel.add(userList);
        panel.add(receive);
        panel.add(scroller);
        panel.add(send);
        panel.add(sendBut);

        setUpNetworking();

        Thread reader = new Thread(new MyReader());
        reader.start();

        frame.getContentPane().add(BorderLayout.CENTER, panel);
        frame.setSize(600, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    private void setUpNetworking() {
        try {
            Socket socket = new Socket("127.0.0.1", 4342);
            inputStream = new ObjectInputStream(socket.getInputStream());
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("networking set up");
        } catch (Exception ex) {
            System.out.println("Can't connect to the server");
            ex.printStackTrace();
        }

    }

    private class MyReader implements Runnable {

        StringBuilder stringBuilder = new StringBuilder();
        Object object = null;
        boolean first = true;
        boolean connect = false;
        int counter = 0;

        @Override
        public void run() {
            try {
                while ((object = inputStream.readObject()) != null) {

                    System.out.println("Got an object from the server");
                    System.out.println(object.getClass());
                    String name = (String) object;
                    String[] strings = name.split("\n");
                    if (first) {
                        if (!users.contains(strings[0])) {
                            users.add(strings[0]);
                            userList.append(strings[0]);
                            userList.append("\n");
                        }
                    }
                    if (first) {
                        stringBuilder.append(name);
                        first = false;
                    } else {
                        stringBuilder.append(name);
                        stringBuilder.append("\n");
                        if (name.equals("Connect")) {
                            connect = true;
                        }
                        first = true;
                    }
                    if (!connect && first) {
                        receive.append(stringBuilder.toString());
                    } else {
                        connect = false;
                    }
                    counter++;
                    if (counter == 2) {
                        stringBuilder.setLength(0);
                        counter = 0;
                    }

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
