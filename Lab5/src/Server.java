import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {

    private ArrayList<ObjectOutputStream> clientOutputStreams;
    private ArrayList<String> users = new ArrayList<>();


    public class ClientHandler implements Runnable {
        ObjectInputStream inputStream;
        Socket socket;

        ClientHandler(Socket clientSocket) {
            try {
                socket = clientSocket;
                inputStream = new ObjectInputStream(socket.getInputStream());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public void run() {
            StringBuilder str = new StringBuilder();
            Object mes1 = null;
            Object mes2 = null;
            try {
                while ((mes1 = inputStream.readObject()) != null) {
                    mes2 = inputStream.readObject();
                    if (mes2.equals("Connect") && !users.contains((String) mes1)) {
                        users.add((String) mes1);

                        for (int i = 0; i < users.size() - 1; i++) {
                            str.append(users.get(i)).append("\n");
                        }
                        mes1 = str.toString();
                        System.out.println("User connected");
                    }
                    str.setLength(0);
                    tellEveryone(mes1, mes2);
                }
            } catch (Exception ex) {
                System.out.println("User disconnected");
            }
        }
    }

    public static void main(String[] args) {
        new Server().start();
    }

    private void start() {
        clientOutputStreams = new ArrayList<>();
        try {
            ServerSocket serverSocket = new ServerSocket(4342);
            while (true) {
                Socket clientSocket = serverSocket.accept();
                ObjectOutputStream writer = new ObjectOutputStream(clientSocket.getOutputStream());
                clientOutputStreams.add(writer);

                Thread thread = new Thread(new ClientHandler(clientSocket));
                thread.start();
                System.out.println("User connected");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void tellEveryone(Object obj1, Object obj2) {
        for (Object clientOutputStream : clientOutputStreams) {
            try {
                ObjectOutputStream writer = (ObjectOutputStream) clientOutputStream;
                writer.writeObject(obj1);
                writer.writeObject(obj2);
                writer.flush();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
