import java.util.Random;
import java.util.Scanner;

public class GameField {

    private Cell[][] field;

    private int x = 9;
    private int y = 9;
    private int mines = 10;
    private boolean first = true;

    GameField(int new_x, int new_y, int new_mines) {
        if (new_x > 20) {
            System.out.println("X number is too big, x set to 9");
        } else if (new_x < 5) {
            System.out.println("X number is too small, x set to 9");
        } else {
            x = new_x;
        }
        if (new_y > 20) {
            System.out.println("Y number is too big, y set to 9");
        } else if (new_y < 5) {
            System.out.println("Y number is too small, y set to 9");
        } else {
            y = new_y;
        }
        if (new_mines >= x * y) {
            System.out.println("Mine number is too big, number set to 10");
        } else if (new_y < 1) {
            System.out.println("Mine number is too small, number set to 10");
        } else {
            mines = new_mines;
        }

        field = new Cell[x][y];

        Random rand = new Random(System.currentTimeMillis());

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                field[i][j] = new Cell(i, j, 0);
            }
        }

        for (int i = 0; i < mines; i++) {
            int x_rnd = rand.nextInt(x);
            int y_rnd = rand.nextInt(y);
            while (field[x_rnd][y_rnd].getState() != 0) {
                x_rnd = rand.nextInt(x);
                y_rnd = rand.nextInt(y);
            }
            field[x_rnd][y_rnd].setState(1);
        }

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (field[i][j].getState() == 0) {
                    int count = 0;
                    if (i != 0) {
                        count += field[i - 1][j].getState();
                        if (j != 0) {
                            count += field[i - 1][j - 1].getState();
                        }
                        if (j != y - 1) {
                            count += field[i - 1][j + 1].getState();
                        }
                    }
                    if (i != x - 1) {
                        count += field[i + 1][j].getState();
                        if (j != 0) {
                            count += field[i + 1][j - 1].getState();
                        }
                        if (j != y - 1) {
                            count += field[i + 1][j + 1].getState();
                        }
                    }
                    if (j != 0) {
                        count += field[i][j - 1].getState();
                    }
                    if (j != y - 1) {
                        count += field[i][j + 1].getState();
                    }

                    if (count > 0) field[i][j].setMines(count);
                }
            }
        }
    }

    public void print() {

        System.out.print("Mines left:");
        System.out.println(mines);

        System.out.print(" ");
        for (int i = 0; i < y; i++) {
            System.out.print(" ");
            System.out.print(i);
        }

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (j == 0) {
                    System.out.println("");
                    System.out.print(i);
                }
                if (field[i][j].getOpened()) {
                    if (field[i][j].getState() == 0) {
                        System.out.print(" ");
                        System.out.print(field[i][j].getMines());
                    } else if (field[i][j].getState() == 1) {
                        System.out.print(" ");
                        System.out.print("*");
                    }
                } else {
                    if (field[i][j].isFlag()) {
                        System.out.print(" ");
                        System.out.print("P");
                    } else {
                        System.out.print(" ");
                        System.out.print(".");
                    }
                }
            }
        }
        System.out.println("");
    }


    public void startingTurn() {
        boolean flag = false;
        System.out.println("Please write coordinates: x y");
        Scanner s = new Scanner(System.in);
        String str;
        String[] data;
        int x_tmp = 0, y_tmp = 0;
        while (!flag) {
            str = s.nextLine();
            data = str.split(" ");
            if (data.length != 2) System.out.println("Wrong arguments, try again...");
            else {
                x_tmp = Integer.parseInt(data[0]);
                y_tmp = Integer.parseInt(data[1]);
                if (x_tmp < 0 || x_tmp >= x || y_tmp < 0 || y_tmp >= y)
                    System.out.println("Wrong coordinates, try again...");
                else flag = true;
            }
        }

        firstTurn(x_tmp, y_tmp);
    }

    public int turn() {
        boolean flag = false;
        System.out.println("Please write coordinates and command: open/flag x y");
        Scanner scanner = new Scanner(System.in);
        String str;
        String[] data = new String[3];
        int x_tmp = 0, y_tmp = 0;
        while (!flag) {
            str = scanner.nextLine();

            data = str.split(" ");
            if (data.length != 3) System.out.println("Wrong arguments, try again...: open/flag x y");
            else {
                x_tmp = Integer.parseInt(data[1]);
                y_tmp = Integer.parseInt(data[2]);
                if (!data[0].equals("flag") && !data[0].equals("open") || x_tmp < 0 || x_tmp >= x || y_tmp < 0 || y_tmp >= y || field[x_tmp][y_tmp].getOpened())
                    System.out.println("Wrong coordinates, try again...:  open/flag x y");
                else flag = true;
            }
        }

        return new_turn(x_tmp, y_tmp, data[0].equals("flag"));
    }

    public void boom() {
        openAllBoom();
        print();
        System.out.println("BOOOOOOOOOOOOOOOOOOOOOOOOOOOOM\nYOU LOST");
    }

    private void openAllBoom() {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (!field[i][j].getOpened() && field[i][j].getState() != 1)
                    field[i][j].setOpened();
                if (field[i][j].getState() == 1) field[i][j].setState(3);
                if (field[i][j].isFlag()) field[i][j].setFlag();
            }
        }
    }

    public void win() {
        openAll();
        print();
        System.out.println("WOW\nThat was great, you won");
    }

    private void openAll() {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (!field[i][j].getOpened())
                    field[i][j].setOpened();
                if (field[i][j].isFlag()) field[i][j].setFlag();
            }
        }
    }

    private void open(int i, int j) {
        if (field[i][j].getState() == 0 && field[i][j].getMines() == 0 && !field[i][j].getOpened()) {
            field[i][j].setOpened();
            if (i != 0) {
                open(i - 1, j);
                if (j != 0) {
                    open(i - 1, j - 1);
                }
                if (j != y - 1) {
                    open(i - 1, j + 1);
                }
            }
            if (i != x - 1) {
                open(i + 1, j);
                if (j != 0) {
                    open(i + 1, j - 1);
                }
                if (j != y - 1) {
                    open(i + 1, j + 1);
                }
            }
            if (j != 0) {
                open(i, j - 1);
            }
            if (j != y - 1) {
                open(i, j + 1);
            }
        }
        if (field[i][j].getState() == 0 && !field[i][j].getOpened())
            field[i][j].setOpened();
    }

    int getX() {
        return x;
    }

    int getY() {
        return y;
    }

    public int getMines() {
        return mines;
    }

    public int click(int x, int y, boolean flag) {
        int res = 0;
        if (first) {
            firstTurn(x, y);
            first = false;
        }
        else {
            res = new_turn(x, y, flag);
        }

        return res;
    }

    private int new_turn(int x_tmp, int y_tmp, boolean flag) {
        if (flag) {
            if (field[x_tmp][y_tmp].getState() == 1) {
                mines--;
            }
            field[x_tmp][y_tmp].setFlag();
        } else {
            if (field[x_tmp][y_tmp].getState() == 1 && !field[x_tmp][y_tmp].isFlag()) {
                field[x_tmp][y_tmp].setOpened();
                field[x_tmp][y_tmp].setState(3);
                return 1;
            }
            else if (field[x_tmp][y_tmp].getMines() == 0) open(x_tmp, y_tmp);
            else field[x_tmp][y_tmp].setOpened();

        }
        if (mines == 0) {
            return 2;
        }

        return 0;

    }

    private void firstTurn(int x_tmp, int y_tmp) {
        if (field[x_tmp][y_tmp].getState() == 1) {
            field[x_tmp][y_tmp].setState(0);
            field[x_tmp][y_tmp].setOpened();

            int count = 0;

            if (x_tmp != 0) {
                if (field[x_tmp - 1][y_tmp].getState() == 0) {
                    field[x_tmp - 1][y_tmp].setMines(field[x_tmp - 1][y_tmp].getMines() - 1);
                }
                else count++;
                if (y_tmp != 0) {
                    if (field[x_tmp - 1][y_tmp - 1].getState() == 0) {
                        field[x_tmp - 1][y_tmp - 1].setMines(field[x_tmp - 1][y_tmp - 1].getMines() - 1);
                    }
                    else count++;
                }
                if (y_tmp != y - 1) {
                    if (field[x_tmp - 1][y_tmp + 1].getState() == 0) {
                        field[x_tmp - 1][y_tmp + 1].setMines(field[x_tmp - 1][y_tmp + 1].getMines() - 1);
                    }
                    else count++;
                }
            }
            if (x_tmp != x - 1) {
                if (field[x_tmp + 1][y_tmp].getState() == 0) {
                    field[x_tmp + 1][y_tmp].setMines(field[x_tmp + 1][y_tmp].getMines() - 1);
                }
                else count++;
                if (y_tmp != 0) {
                    if (field[x_tmp + 1][y_tmp - 1].getState() == 0) {
                        field[x_tmp + 1][y_tmp - 1].setMines(field[x_tmp + 1][y_tmp - 1].getMines() - 1);
                    }else count++;
                }
                if (y_tmp != y - 1) {
                    if (field[x_tmp + 1][y_tmp + 1].getState() == 0) {
                        field[x_tmp + 1][y_tmp + 1].setMines(field[x_tmp + 1][y_tmp + 1].getMines() - 1);
                    } else count++;
                }
            }
            if (y_tmp != 0) {
                if (field[x_tmp][y_tmp - 1].getState() == 0) {
                    field[x_tmp][y_tmp - 1].setMines(field[x_tmp][y_tmp - 1].getMines() - 1);
                } else count++;
            }
            if (y_tmp != y - 1) {
                if (field[x_tmp][y_tmp + 1].getState() == 0) {
                    field[x_tmp][y_tmp + 1].setMines(field[x_tmp][y_tmp + 1].getMines() - 1);
                } else count++;
            }

            field[x_tmp][y_tmp].setMines(count);
            Random rand = new Random(System.currentTimeMillis());

            int x_rnd = rand.nextInt(x);
            int y_rnd = rand.nextInt(y);
            while (field[x_rnd][y_rnd].getState() != 0 && (x_rnd != x_tmp && y_rnd != y_tmp)) {
                x_rnd = rand.nextInt(x);
                y_rnd = rand.nextInt(y);
            }
            field[x_rnd][y_rnd].setState(1);

            if (x_rnd != 0) {
                field[x_rnd - 1][y_rnd].setMines(field[x_rnd - 1][y_rnd].getMines() + 1);
                if (y_rnd != 0) {
                    field[x_rnd - 1][y_rnd - 1].setMines(field[x_rnd - 1][y_rnd - 1].getMines() + 1);
                }
                if (y_rnd != y - 1) {
                    field[x_rnd - 1][y_rnd + 1].setMines(field[x_rnd - 1][y_rnd + 1].getMines() + 1);
                }
            }
            if (x_rnd != x - 1) {
                field[x_rnd + 1][y_rnd].setMines(field[x_rnd + 1][y_rnd].getMines() + 1);
                if (y_rnd != 0) {
                    field[x_rnd + 1][y_rnd - 1].setMines(field[x_rnd + 1][y_rnd - 1].getMines() + 1);
                }
                if (y_rnd != y - 1) {
                    field[x_rnd + 1][y_rnd + 1].setMines(field[x_rnd + 1][y_rnd + 1].getMines() + 1);
                }
            }
            if (y_rnd != 0) {
                field[x_rnd][y_rnd - 1].setMines(field[x_rnd][y_rnd - 1].getMines() + 1);
            }
            if (y_rnd != y - 1) {
                field[x_rnd][y_rnd + 1].setMines(field[x_rnd][y_rnd + 1].getMines() + 1);
            }
        }
        else if (field[x_tmp][y_tmp].getMines() == 0) open(x_tmp, y_tmp);
        else {
            field[x_tmp][y_tmp].setOpened();
        }

    }

    public int status(int i, int j) {
        if (field[i][j].getOpened()) {
            if (field[i][j].getState() == 3) return -4;
            if (field[i][j].getState() == 1) return -2;
            if (field[i][j].getState() == 0) {
                return field[i][j].getMines();
            }
        }
        else {
            if (field[i][j].isFlag()) return -3;
            return -1;
        }
        return 10;
    }

    public void setFirst() {
        first = true;
    }
}
