import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

public class MyPanel extends JPanel {

    private JButton[][] fieldBut;
    private GameField field;

    public MyPanel(GameField f) {
        setLayout(null);
        field = f;
        fieldBut = new JButton[field.getX()][field.getY()];
        makeField();

        for (int i = 0; i < field.getX(); i++) {
            for (int j = 0; j < field.getY(); j++) {
                fieldBut[i][j].setActionCommand(i + "," + j);
                int x = i;
                int y = j;

                fieldBut[x][y].addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        int res = -1;
                        if (e.getButton() == MouseEvent.BUTTON1) {
                            res = field.click(x, y, false);
                        } else if (e.getButton() == MouseEvent.BUTTON3) {
                            res = field.click(x, y, true);
                        }
                        update();

                        if (res == 1) {
                            JOptionPane.showMessageDialog(null, "BOOOOOM, you lost");
                        } else if (res == 2) {
                            JOptionPane.showMessageDialog(null, "WOW, you won!");
                        } else if (res == -1) {
                            JOptionPane.showMessageDialog(null, "?????????????????");
                        }

                    }

                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {

                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }
                });
            }
        }
    }

    private void update() {
        ImageIcon image;
        for (int i = 0; i < field.getX(); i++) {
            for (int j = 0; j < field.getY(); j++) {
                int res = field.status(i, j);

                switch (res) {
                    case -1: {
                        image = makeIcon("closed.png");
                        break;
                    }
                    case -2: {
                        image = makeIcon("bomb.png");
                        break;
                    }
                    case -3: {
                        image = makeIcon("flag.png");
                        break;
                    }
                    case -4: {
                        image = makeIcon("exploded.png");
                        break;
                    }
                    case 0: {
                        image = makeIcon("zero.png");
                        break;
                    }
                    case 1: {
                        image = makeIcon("one.png");
                        break;
                    }
                    case 2: {
                        image = makeIcon("two.png");
                        break;
                    }
                    case 3: {
                        image = makeIcon("three.png");
                        break;
                    }
                    case 4: {
                        image = makeIcon("four.png");
                        break;
                    }
                    case 5: {
                        image = makeIcon("five.png");
                        break;
                    }
                    case 6: {
                        image = makeIcon("six.png");
                        break;
                    }
                    case 7: {
                        image = makeIcon("seven.png");
                        break;
                    }
                    case 8: {
                        image = makeIcon("eight.png");
                        break;
                    }
                    default: {
                        image = makeIcon("what.png");
                        break;
                    }
                }
                fieldBut[i][j].setIcon(image);
            }
        }

    }

    private void makeField() {

        ImageIcon image;
        for (int i = 0; i < field.getX(); i++) {
            for (int j = 0; j < field.getY(); j++) {
                image = makeIcon("closed.png");
                fieldBut[i][j] = new JButton();
                fieldBut[i][j].setIcon(image);
                fieldBut[i][j].setBounds(16 * i, 16 * j + 30, 16, 16);
                add(fieldBut[i][j]);
            }
        }

    }

    private ImageIcon makeIcon(String s) {
        URL image = MyPanel.class.getResource(s);
        if (image != null) {
            return new ImageIcon(image);
        } else {
            System.out.println("File does not exist (" + s + ")");
            return null;
        }


    }
}
