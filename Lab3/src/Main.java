import java.util.Scanner;

public class Main {

    private static GameField field;
    private static View view;

    public static void main(String[] args) {
        init();
        if (view == null) inGame();
    }

    private static void init() {
        System.out.println("Enter the field size and number of mines: x y mines");
        Scanner s = new Scanner(System.in);
        String str, res;
        String[] data;
        boolean flag = false;
        int x = 0, y = 0, mines = 0;
        while (!flag) {
            str = s.nextLine();
            data = str.split(" ");
            try {
                x = Integer.parseInt(data[0]);
                y = Integer.parseInt(data[1]);
                mines = Integer.parseInt(data[2]);
                flag = true;
            } catch (NumberFormatException e) {
                System.out.println("Wrong format, try again...");
            }
        }
        System.out.println("Gui or text?: gui/text");
        flag = false;
        while (!flag) {
            str = s.nextLine();
            try {
                res = str.toLowerCase();
                switch (res) {
                    case "gui":
                        field = new GameField(x, y, mines);
                        view = new View(field);
                        break;
                    case "text":
                        field = new GameField(x, y, mines);
                        break;
                    default:
                        throw new Exception();
                }
                flag = true;
            } catch (Exception e) {
                System.out.println("Wrong format, try again...");
            }
        }
        if (view == null) field.print();
    }

    private static void inGame() {
        int res = 0;

        field.startingTurn();
        field.print();
        while (res == 0) {
            res = field.turn();
            field.print();
        }

        if (res == 1) {
            field.boom();
        }
        if (res == 2) {
            field.win();
        }
    }
}
