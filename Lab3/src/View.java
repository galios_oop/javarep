import javax.swing.*;

public class View {

    private int mineNum;
    private JFrame wind;
    private GameField field;
    private JButton button;
    private MyPanel panel;

    public View(GameField f) {
        wind = new JFrame("Minesweaper");
        field = f;
        mineNum = f.getMines();
        wind.setSize(20 * field.getX(), 20 * field.getY() + 50);

        panel = new MyPanel(field);
        wind.add(panel);

        button = new JButton("New Game");
        button.setBounds(0, 0, 100, 32);
        button.addActionListener(e -> {
            newGame(field.getX(), field.getY(), mineNum);
            panel.add(button);
        });
        panel.add(button);
        wind.setLocationRelativeTo(null);
        wind.setResizable(false);
        wind.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        wind.setVisible(true);
    }

    private void newGame(int x, int y, int mines) {

        field = new GameField(x, y, mines);
        MyPanel panel_new = new MyPanel(field);
        field.setFirst();
        wind.remove(panel);
        wind.add(panel_new);
        panel = panel_new;
        wind.revalidate();
        wind.repaint();
    }

}
