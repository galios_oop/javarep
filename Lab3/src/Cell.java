public class Cell {

    private int x;
    private int y;
    private int state = 0; // 0 - not a mine, 1 - mine, 2 - flag, 3 - exploded
    private boolean isFlag = false;
    private boolean opened = false; // 0 - closed, 1 - opened
    private int mines = 0; // 0 - no mines around


    public Cell(int new_x, int new_y, int mine) {
        x = new_x;
        y = new_y;
        if (mine != 0) state = 1;
    }

    public int getState() {
        return state;
    }

    public void setState(int new_state) {
        state = new_state;
    }

    public void setFlag() {
        isFlag = !isFlag;
    }

    public int getMines() {
        return mines;
    }

    public void setMines(int num) {
        mines = num;
    }


    public boolean getOpened() {
        return opened;
    }

    public void setOpened() {
        opened = !opened;
    }

    public boolean isFlag() {
        return isFlag;
    }
}
