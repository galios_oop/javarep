public class Root implements Operation {

    @Override
    public void calculate(String[] str) throws Exception {
        if (Context.stack.empty()){
            throw new Exception("Root: Empty stack error");
        }
        else{
            Context.stack.push(Math.sqrt(Context.stack.pop()));
        }
    }
}
