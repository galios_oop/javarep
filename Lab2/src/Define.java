public class Define implements Operation {

    @Override
    public void calculate(String[] str) {
        Context.defined.put(str[0], Double.valueOf(str[1]));
    }
}
