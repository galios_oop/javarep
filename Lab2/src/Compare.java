public class Compare implements Operation {

    @Override
    public void calculate(String[] str) throws Exception {
        if (Context.stack.size() < 2) {
            throw new Exception("Compare: Not enough elements on stack error");
        }
        else {
            double a = Context.stack.pop();
            double b = Context.stack.pop();

            if (a > b) {
                Context.stack.push(1.0);
            }
            else {
                Context.stack.push(0.0);
            }
        }
    }
}
