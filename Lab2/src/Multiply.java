public class Multiply implements Operation {

    @Override
    public void calculate(String[] str) throws Exception {
        if (Context.stack.size() < 2) {
            throw new Exception("Multiply: Not enough elements on stack error");
        }
        else {
            Context.stack.push(Context.stack.pop() * Context.stack.pop());
        }
    }
}
