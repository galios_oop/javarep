public class Swap implements Operation {

    @Override
    public void calculate(String[] str) throws Exception {
        if (Context.stack.size() < 2) {
            throw new Exception("Swap: Not enough elements on stack error");
        } else {
            double a = Context.stack.pop();
            double b = Context.stack.pop();

            Context.stack.push(a);
            Context.stack.push(b);
        }
    }
}
