import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

public class Creator {

    private static Properties props;

    public static void start() {
        try {
            props = new Properties();
            if (Context.commands.size() > 0) {
                InputStream ist = Creator.class.getResourceAsStream("conf.properties");
                props.load(ist);
                ist.close();
            }
        } catch (IOException ex ) {
            ex.printStackTrace();
        }
    }
    public static Operation getOperation(String str) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        if (isNum(str)) {
            str = "push";
        }
        String name = props.getProperty(str);

        Class<?> new_class = Class.forName(name);
        Object obj = new_class.getDeclaredConstructor().newInstance();
        return  (Operation) obj;
    }

    private static boolean isNum(String str) {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
        return true;
    }
}
