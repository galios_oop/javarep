public class Push implements Operation{

    @Override
    public void calculate(String[] str) throws Exception {
        if (Context.defined.containsKey(str[0])) {
            Context.stack.push(Context.defined.get(str[0]));
        } else {
            Context.stack.push(Double.valueOf(str[0]));
        }
    }
}
