interface Operation {
    void calculate(String[] str) throws Exception;
}
