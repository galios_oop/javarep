import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class CalcMain {

    public static void main(String[] args) {
        try {
            if (args.length == 1) {
                FileInputStream inputStream = new FileInputStream(args[0]);
                Context.commands = getCommands(inputStream);
                inputStream.close();

                Creator.start();
            } else if (args.length == 0) {
                Context.commands = getCommands(System.in);
                Creator.start();
            } else {
                System.out.println("Too many args");
            }

            for (Context.curPos = 0; Context.curPos < Context.commands.size(); Context.curPos++) {
                Operation op;
                String[] str = new String[2];
                boolean is_num = isNum(Context.commands.get(Context.curPos));
                op = Creator.getOperation(Context.commands.get(Context.curPos));

                if (is_num) {
                    str[0] = Context.commands.get(Context.curPos);
                } else if (Context.commands.get(Context.curPos).equals("push")) {
                    str[0] = Context.commands.get(++Context.curPos);
                }
                if (Context.commands.get(Context.curPos).equals("define")) {
                    str[0] = Context.commands.get(++Context.curPos);
                    str[1] = Context.commands.get(++Context.curPos);
                }
                if (Context.commands.get(Context.curPos).equals("[")) {
                    int k = Context.curPos;
                    int r = 0;
                    int r1 = 0, k1 = 0;
                    for (int j = k + 1; j < Context.commands.size(); j++) {
                        if (Context.commands.get(j).equals("]")) {
                            r = j;
                            break;
                        }
                        if (Context.commands.get(j).equals("[")) {
                            k1 = j;
                            break;
                        }
                    }
                    if (k1 != 0) {
                        for (int j = k1; j < Context.commands.size(); j++) {
                            if (Context.commands.get(j).equals("]")) {
                                r1 = j;
                                break;
                            }
                        }
                        for (int j = r1 + 1; j < Context.commands.size(); j++) {
                            if (Context.commands.get(j).equals("]")) {
                                r = j;
                                break;
                            }
                        }
                        str = new String[r - k];
                        for (int i = 0; i < r - k - 1; i++) {
                            str[i] = Context.commands.get(++Context.curPos);
                        }
                        ++Context.curPos;
                    }
                    else {
                        str = new String[r - k];
                        for (int i = 0; i < r - k - 1; i++) {
                            str[i] = Context.commands.get(++Context.curPos);
                        }
                        ++Context.curPos;
                    }
                }

                op.calculate(str);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private static ArrayList<String> getCommands(InputStream ist) {
        Scanner s = new Scanner(ist).useDelimiter("\\A");
        String res = s.hasNext() ? s.next() : "";
        s.close();

        String result = res.toLowerCase();

        result = result.replaceAll("\\s+", " ");
        String[] str = result.split(" ");

        return new ArrayList<>(Arrays.asList(str));
    }

    private static boolean isNum(String str) {
        try {
            double v = Double.parseDouble(str);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }
}
