public class Pop implements Operation{


    @Override
    public void calculate(String[] str) throws Exception {
        if (Context.stack.empty()){
            throw new Exception("Pop: Empty stack error");
        }
        else{
            Context.stack.pop();
        }
    }
}
