public class Cycle implements Operation {

    @Override
    public void calculate(String[] str) throws Exception {
        if (Context.stack.empty()){
            throw new Exception("Cycle: Empty stack error");
        }
        double d = Context.stack.pop();
        if (d == 0) {
            return;
        }

        Operation op;
        String[] s = new String[2];

        while (d != 0) {
            for (int i = 0; i < str.length - 1; i++) {
                boolean is_num = isNum(str[i]);
                op = Creator.getOperation(str[i]);

                if (is_num) {
                    s[0] = str[i];
                }
                else if (str[i].equals("push")) {
                    s[0] = str[++i];
                }
                if (str[i].equals("define")) {
                    s[0] = str[++i];
                    s[1] = str[++i];
                }
                if (str[i].equals("[")) {
                    int k = i;
                    int r = 0;
                    for (int j = k; j < str.length; j++) {
                        if (str[j].equals("]")) {
                            r = j;
                            break;
                        }
                    }
                    s = new String[r - k];
                    for (int l = 0; l < r - k - 1; l++) {
                        s[l] = str[++i];
                    }
                    ++i;
                }
                op.calculate(s);
            }
            d = Context.stack.pop();
        }


    }

    private static boolean isNum(String str) {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
        return true;
    }
}
