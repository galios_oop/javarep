public class Dup implements Operation {

    @Override
    public void calculate(String[] str) throws Exception {

        if (Context.stack.empty()){
            throw new Exception("Dup: Empty stack error");
        }
        else{
            Context.stack.push(Context.stack.peek());
        }
    }
}
