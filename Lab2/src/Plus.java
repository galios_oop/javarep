public class Plus implements Operation {

    @Override
    public void calculate(String[] str) throws Exception {
        if (Context.stack.size() < 2) {
            throw new Exception("Plus: Not enough elements on stack error");
        }
        else {
            Context.stack.push(Context.stack.pop() + Context.stack.pop());
        }
    }
}
