public class Drop implements Operation {

    @Override
    public void calculate(String[] str) throws Exception {
        if (Context.stack.empty()){
            throw new Exception("Drop: Empty stack error");
        }
        else{
            Context.stack.pop();
        }
    }
}
