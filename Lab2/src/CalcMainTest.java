import org.junit.Test;

import static org.junit.Assert.*;

public class CalcMainTest {

    @Test
    public void main() throws Exception{

        String[] args1 = {"input1.txt"};
        String[] args2 = {"input2.txt"};
        String[] args3 = {"input3.txt"};
        String[] args4 = {"input4.txt"};
        String[] args5 = {"input5.txt"};
        String[] args6 = {"input6.txt"};
        String[] args7 = {"input7.txt"};

        String str1 = "5.0\n4.0\n3.0\n2.0\n1.0\n";
        String str2 = "120.0\n";
        String str3 = "1.0\n";
        String str4 = "5.0\n";
        String str6 = "5.0\n" + "4.0\n" + "3.0\n" + "2.0\n" + "1.0\n" + "4.0\n" + "3.0\n" + "2.0\n" + "1.0\n" + "3.0\n" + "2.0\n" + "1.0\n" + "2.0\n" + "1.0\n" + "1.0\n";
        String str5 = "5.0\n4.0\n3.0\n2.0\n1.0\n";
        String str7 = "2.0\n";

        String res1 = ConsoleInterceptor.copyOut(() -> CalcMain.main(args1));
        String res2 = ConsoleInterceptor.copyOut(() -> CalcMain.main(args2));
        String res3 = ConsoleInterceptor.copyOut(() -> CalcMain.main(args3));
        String res4 = ConsoleInterceptor.copyOut(() -> CalcMain.main(args4));
        String res5 = ConsoleInterceptor.copyOut(() -> CalcMain.main(args5));
        String res6 = ConsoleInterceptor.copyOut(() -> CalcMain.main(args6));
        String res7 = ConsoleInterceptor.copyOut(() -> CalcMain.main(args7));

        assertEquals(str1, res1);
        assertEquals(str2, res2);
        assertEquals(str3, res3);
        assertEquals(str4, res4);
        assertEquals(str5, res5);
        assertEquals(str6, res6);
        assertEquals(str7, res7);
    }
}