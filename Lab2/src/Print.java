public class Print implements Operation {

    @Override
    public void calculate(String[] str) throws Exception {
        if (Context.stack.empty()){
            throw new Exception("Print: Empty stack error");
        }
        else{
            double num = Context.stack.peek();
            System.out.println(num);
        }
    }
}
