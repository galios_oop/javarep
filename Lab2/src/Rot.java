public class Rot implements Operation {

    @Override
    public void calculate(String[] str) throws Exception {
        if (Context.stack.size() < 3) {
            throw new Exception("Rot: Not enough elements on stack error");
        }
        else {
            double c = Context.stack.pop();
            double b = Context.stack.pop();
            double a = Context.stack.pop();

            Context.stack.push(b);
            Context.stack.push(c);
            Context.stack.push(a);
        }
    }
}
